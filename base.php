<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

	<!--[if lt IE 8]>
		<div class="alert alert-warning">
			<?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
		</div>
	<![endif]-->


    <!--[if lte IE 8]>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/modernizr/2.8.3/modernizr.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->

	<div id="wrapper" class="stickyfooter st-container st-effect-7">
        <div class="st-pusher">
            <?php
            do_action('get_header');
            // Use Bootstrap's navbar if enabled in config.php
            if (current_theme_supports('bootstrap-top-navbar')) {
                get_template_part('templates/header-top-navbar');
            } else {
                get_template_part('templates/header');
            }
            ?>

            <?php if (wp_is_mobile()): ?>
			 <div id="sidebar-wrapper" class="sidebar-wrapper navbar-mobile st-menu st-effect-7 hidden-lg hidden-md hidden-sm" role="navigation">
                <?php get_template_part('templates/mobile', 'nav'); ?>
			 </div>
            <?php endif; ?>
			<div id="content-wrapper" class="st-content">
				<div class="wrap <?php // echo get_roots_theme_size(); ?> st-content-inner" role="document">
					<div class="content row">
						<main class="main <?php echo roots_main_class(); ?>" role="main">

							<?php include roots_template_path(); ?>

						</main><!-- /.main -->

						<?php if (roots_display_sidebar()) : ?>
							<aside class="sidebar <?php echo roots_sidebar_class(); ?>" role="complementary">
								<?php include roots_sidebar_path(); ?>
							</aside><!-- /.sidebar -->
						<?php endif; ?>
					</div><!-- /.content -->
				</div><!-- /.wrap -->
			</div><!-- /.content-wrapper -->
		</div><!-- /.wrapper -->
	</div><!-- /.stickyfooter -->
	<?php get_template_part('templates/footer'); ?>


</body>
</html>
