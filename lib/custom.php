<?php
/**
 * Custom functions
 */

//========= Inline header ===============================================
function inline_header_code() { ?>

    <script type="text/javascript">
        new WOW().init();
    </script>
<?php }
add_action( 'wp_footer', 'inline_header_code', 50 );

//=========| Add your scripts here |====================================
function roots() {

    //OPTIONAL SCRIPTS
    //--retina.js----Automatically looks for retina images using name@2x.jpg syntax
       // wp_enqueue_script('retina-js', '//cdnjs.cloudflare.com/ajax/libs/retina.js/1.3.0/retina.min.js', array(), null, true );


      //wp_register_script('isotope', get_template_directory_uri() . '/assets/js/vendor/isotope.pkgd.min.js', array(), null, false);  //if use isotope (google it)
      // wp_register_script('angular', get_template_directory_uri() . '/assets/js/vendor/angular.min.js', array(), null, false);
}
add_action('wp_enqueue_scripts', 'roots');


//==========| GALLERY + LIGHTBOX |=========================[HOOK]=======
function magnific_popup_files() {
    wp_enqueue_style( 'magnific', get_template_directory_uri() . '/assets/css/vendor/magnific-popup-gallery.css' );
    wp_enqueue_script( 'magnific', get_template_directory_uri() .'/assets/js/vendor/magnific-popup-gallery.min.js', array('jquery'), false, true );
}

//add_action( 'run_gallery', 'magnific_popup_files' );


//==========| FULL PAGE SLIDER (fullPage.js)  |============[HOOK]=======
function fullpageslider() {
    wp_enqueue_style( 'fullpage-css', get_template_directory_uri() . '/assets/css/vendor/jquery.fullPage.css' );
    wp_enqueue_script( 'fullpage-slimscroll', get_template_directory_uri() .'/assets/js/vendor/jquery.slimscroll.min.js');
    wp_enqueue_script( 'fullpag-core', get_template_directory_uri() .'/assets/js/vendor/jquery.fullPage.min.js', array('jquery'));

    wp_enqueue_script( 'fullpag-helper', get_template_directory_uri() .'/assets/js/vendor/jquery.easings.min.js');


}

add_action( 'run_fullpage', 'fullpageslider' );  //hooked to the run_fullpage action use --> do_action( 'run_fullpage' );

//==========| LOGIN FORM SHORTCODE  |==================================
//  use syntax -- > [loginform redirect="http://my-redirect-url.com"]
function login_form_shortcode( $atts, $content = null ) {

    extract( shortcode_atts( array(
        'redirect' => ''
    ), $atts ) );

    $form = '';
    if (!is_user_logged_in()) {
        if($redirect) {
            $redirect_url = $redirect;
        } else {
            $redirect_url = get_permalink();
        }
        $form = wp_login_form(array('echo' => false, 'redirect' => $redirect_url ));
    }
    return $form;
}
add_shortcode('loginform', 'login_form_shortcode');


//===========| LOGIN CUSTOMIZER |======================================
function brand_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png);
            padding-bottom: 30px;
            width: 300px;
            background-size: 80%;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'brand_login_logo' );

function brand_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'brand_login_logo_url' );

//=========| CUSTOM SMTP SETTINGS |====================================

add_action('phpmailer_init','send_smtp_email');
function send_smtp_email( $phpmailer )
{
    // Define that we are sending with SMTP
    $phpmailer->isSMTP();

    // The hostname of the mail server
    $phpmailer->Host = "smtp.mandrillapp.com";

    // Use SMTP authentication (true|false)
    $phpmailer->SMTPAuth = true;

    // SMTP port number - likely to be 25, 465 or 587
    $phpmailer->Port = "587";

    // Username to use for SMTP authentication
    $phpmailer->Username = "alex@graphicfusiondesign.com";

    // Password to use for SMTP authentication
    $phpmailer->Password = "06UbIUos5HcZaXsKuWV1sA";

    // The encryption system to use - ssl (deprecated) or tls
    $phpmailer->SMTPSecure = "tls";

    $phpmailer->From = "info@graphicfusiondesign.com";
    $phpmailer->FromName = "Your Website";
}



//========= FOOTER MENU ===============================================
function register_footer_menu() {
    register_nav_menu('footer-menu',__( 'Footer Menu' ));
}
add_action( 'init', 'register_footer_menu' );

//========= SECONDARY MOBILE MENU =====================================
function register_secmobile_menu() {
    register_nav_menu('secmobile-menu',__( 'Second Mobile Menu' ));
}
add_action( 'init', 'register_secmobile_menu' );

//========= FIX The double nav items ==================================
/* The code below finds the menu item with the class "[CPT]-menu-item" and adds another “current_page_parent”   class to it. Furthermore, it removes the “current_page_parent” from the blog menu item, if this is present. (also code was added to nav.php to support this fix). More at: http://vayu.dk/highlighting-wp_nav_menu-ancestor-children-custom-post-types/
*/

add_filter('nav_menu_css_class', 'current_type_nav_class', 10, 2);
function current_type_nav_class($classes, $item) {
    // Get post_type for this post
    $post_type = get_query_var('post_type');

    // Removes current_page_parent class from blog menu item
    if ( get_post_type() == $post_type )
        $classes = array_filter($classes, "get_current_value" );

    // Go to Menus and add a menu class named: {custom-post-type}-menu-item
    // This adds a current_page_parent class to the parent menu item
    if( in_array( $post_type.'-menu-item', $classes ) )
        array_push($classes, 'current_page_parent');

    return $classes;
}
function get_current_value( $element ) {
    return ( $element != "current_page_parent" );
}

//========== WOOCOMERCE SPECIFIC =======================================
add_theme_support( 'woocommerce' );
if ( function_exists('is_woocommerce')) {


/**
 * Optimize WooCommerce Scripts
 * Remove WooCommerce Generator tag, styles, and scripts from non WooCommerce pages.
 */
add_action( 'wp_enqueue_scripts', 'child_manage_woocommerce_styles', 99 );

function child_manage_woocommerce_styles() {
    //remove generator meta tag
    remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );

	//dequeue scripts and styles
	if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
		wp_dequeue_style( 'woocommerce-layout' );
		wp_dequeue_style( 'woocommerce-smallscreen' );
		wp_dequeue_style( 'woocommerce-general' );
		wp_dequeue_script( 'wc-add-to-cart' );
		wp_dequeue_script( 'wc-cart-fragments' );
		wp_dequeue_script( 'woocommerce' );
		wp_dequeue_script( 'jquery-blockui' );
		wp_dequeue_script( 'jquery-placeholder' );
	}


}
// Change number or products per row to 3
    add_filter('loop_shop_columns', 'loop_columns');
    if (!function_exists('loop_columns')) {
        function loop_columns() {
            return 3; // 3 products per row
        }
    }

// Remove WooCommerce Updater
    remove_action('admin_notices', 'woothemes_updater_notice');

}

/* 
 * Options Page
 * Add ACF items to an options page 
 * */

if ( function_exists('acf_add_options_page') ) {

    acf_add_options_page(
        array(
            'page_title'    => 'Site Options',
            'menu_title'    => 'Site Options',
            'menu_slug'     => 'site-options',
            'capability'    => 'edit_posts',
            'parent_slug'   => '',
            'position'      => false,
            'icon_url'      => false
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title'    => 'Testimonials',
            'menu_title'    => 'Testimonials',
            'menu_slug'     => 'site-options-testimonials',
            'capability'    => 'edit_posts',
            'parent_slug'   => 'site-options',
            'position'      => false,
            'icon_url'      => false
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title'    => 'Header',
            'menu_title'    => 'Header',
            'menu_slug'     => 'site-options-header',
            'capability'    => 'edit_posts',
            'parent_slug'   => 'site-options',
            'position'      => false,
            'icon_url'      => false
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title'    => 'Footer',
            'menu_title'    => 'Footer',
            'menu_slug'     => 'site-options-footer',
            'capability'    => 'edit_posts',
            'parent_slug'   => 'site-options',
            'position'      => false,
            'icon_url'      => false
        )
    );    
}
if( function_exists('acf_add_local_field_group') ){
	acf_add_local_field_group(
		array (
			'key' => 'testimonial-repeater',
			'title' => 'Testimonials',
			'fields' => array (
				array (
					'key' => 'testimonials',
					'label' => 'Testimonials',
					'name' => 'testimonials',
					'type' => 'repeater',
					'placeholder' => '',
				)
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'site-options-testimonials',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
		)
	);
	// Adds repeater rows to testimonials group
	acf_add_local_field_group(
		array (
			'key' => 'repeater-row',
			'title' => 'Row',
			'parent' => 'testimonials',
			'fields' => array (
				array (
					'key' => 'name',
					'label' => 'Name',
					'name' => 'name',
					'type' => 'text',
					'parent' => 'testimonials',
					'placeholder' => 'John Egglebert'
				),
				array (
					'key' => 'quote',
					'label' => 'Quote',
					'name' => 'quote',
					'type' => 'textarea',
					'parent' => 'testimonials',
					'placeholder' => 'Joshua\'s help was unmatched. A light in a dark world.'
				)
			)
		)
	);
	acf_add_local_field_group(
		array (
			'key' => 'property-gallery',
			'title' => 'Gallery',
			'fields' => array (
				array(
					'key' => 'prop-gallery',
					'label' => 'Name',
					'name' => 'prop-gallery',
					'type' => 'gallery',
					'min' => 0,
					'max' => 0,
					'preview_size' => 'thumbnail',
					'library' => 'all',
					'min_width' => 0,
					'min_height' => 0,
					'min_size' => 0,
					'max_width' => 0,
					'max_height' => 0,
					'max_size' => 0,
					'mime_types' => ''	
				)
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'property',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
		)
	);
	acf_add_local_field_group(array(
		'key' => 'page-info',
		'title' => 'Page Info',
		'fields' => array (
			array (
				'key' => 'page-name',
				'label' => 'Page Name',
				'name' => 'page-name',
				'type' => 'text'
			)
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
				),
			),
		),
	));

}

function pagination_nav() {
	global $wp_query;
	$total = $wp_query->max_num_pages;
	// only bother with the rest if we have more than 1 page!
	if ( $total > 1 )  {
	     // get the current page
	     if ( !$current_page = get_query_var('paged') )
	          $current_page = 1;
	     // structure of "format" depends on whether we're using pretty permalinks
	     if( get_option('permalink_structure') ) {
		     $format = '&paged=%#%';
	     } else {
		     $format = 'page/%#%/';
	     }
	     $array = paginate_links(array(
			'base'     => get_pagenum_link(1) . '%_%',
			'format'   => $format,
			'current'  => $current_page,
			'total'    => $total,
			'show_all' => true,
			'type'     => 'array'
	     ));
		return $array;
	}
}