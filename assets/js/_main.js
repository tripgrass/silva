/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can 
 * always reference jQuery with $, even when in .noConflict() mode.
 *
 * Google CDN, Latest jQuery
 * To use the default WordPress version of jQuery, go to lib/config.php and
 * remove or comment out: add_theme_support('jquery-cdn');
 * ======================================================================== */

(function($) {
  

// Use this variable to set up the common and page specific functions. If you 
// rename this variable, you will also need to rename the namespace below.
var Roots = {

 // All pages
  common: {
    init: function() {

      // Sliding menu
      var padding= $('.stickyfooter').css('padding');

      $('button.navbar-toggle').click(function () {

          $('#wrapper').toggleClass('active');
          $(this).toggleClass('active');
          var target = $(this).attr('data-target');

          if ($('#wrapper').hasClass('active')) {

            $('header').css('left','0px');
            $('header button.navbar-toggle').css({'left':'10px', 'right':'initial'});
            $('.navbar-brand').addClass('hide');

            // Compensate for our stickyfooter being a bitch
            $('footer').addClass('hide');
            $('.stickyfooter').css({'padding':'0', 'height':'100%'});
            $('.st-menu').removeClass('invisible');

            // Hide elements and display only the one that is toggled
            $('button.navbar-toggle').addClass('hide');
            $(this).removeClass('hide');
            $('#sidebar-wrapper .mobile-menu').addClass('hide');
            $(target).removeClass('hide');


          } else {

            $('header').css('left','0px');
            $('header button.navbar-toggle').css({'right':'10px', 'left':'initial'});
            $('header button.navbar-toggle.toggle-two').css({'right':'58px', 'left':'initial'});

            $('.navbar-brand').removeClass('hide');

            // Turn our sticky footer back on
            $('footer').removeClass('hide')
            $('.stickyfooter').css('padding',padding);

            // Hide menu and show toggles
            $('.st-menu').addClass('invisible')
            $(target).addClass('hide');
            $('button.navbar-toggle').removeClass('hide');

            window.setTimeout(function() {   //need this timeout otherwise it shows up weird
                $('.stickyfooter').css('height','auto');
            },300);
          }

        });

        // mobile dropdown menu
        $('.navbar-mobile .dropdown:not(.on) .dropdown-toggle').click(function (event) {
            if ( ! $(this).parent().hasClass('on') ) {
                event.preventDefault();
                $(this).parent().toggleClass('on');
            }
        });

        $(window).resize(function() {
          if ( $(window).width() >= 769 ) {
            $('#wrapper').removeClass('active');
          }
        });
    },
    finalize: function() { }
  },
  // Home page
  home: {
    init: function() {
      // JavaScript to be fired on the home page
    }
  },
  // About us page, note the change from about-us to about_us.
  about_us: {
    init: function() {
      // JavaScript to be fired on the about us page
    }
  }
};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var UTIL = {
  fire: function(func, funcname, args) {
    var namespace = Roots;
    funcname = (funcname === undefined) ? 'init' : funcname;
    if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function() {
    UTIL.fire('common');

    $.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
      UTIL.fire(classnm);
    });
  }
};

$(document).ready(UTIL.loadEvents);

$(document).ready(function() { // USED FOR MOTULITPLE THINGS

    //-----| Smooth Scroll to anchors
    $('a[href*=#]:not([href=#description], [href=#features], [href=#details])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    //--------| Animate our header on scroll
    $(window).scroll(function() {
        if ($(this).scrollTop() > 1){
            $('.navbar-fixed-top').addClass("sticky");
        }
        else{
            $('.navbar-fixed-top').removeClass("sticky");
        }
    });

    //-------| Move navbar to the top upon scroll option use navbar-static-top'
    // see http://www.bootply.com/107973
//    $('#nav').affix({
//        offset: {
//            top: $('header').height()
//        }
//    });


    //-------| Put text inside input boxes --------
    $('input[type="text"]').each(function() {
        $(this).attr("placeholder", $(this).attr("value")).removeAttr("value");
    });

    //-------| initiates "Slick.js" slider for property images --------
	$('.gallery-slider').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		prevArrow: '.nav-diamond.left',
		nextArrow: '.nav-diamond.right',
	});

    //-------| initiates popup lightbox images --------
	$('.popup-link').magnificPopup({ 
	  	type: 'image',
		callbacks: {
		  open: function() {
		    $('html').css('margin-right', 0); // hack to correct magnific margin addition
		  }
		}
	});

	$('.property-tabs a[href="#description"] , .property-tabs a[href="#features"] , .property-tabs a[href="#details"]').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show')
	})

});

})(jQuery); // Fully reference jQuery after this point.
