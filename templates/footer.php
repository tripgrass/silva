<footer class="content-info footer tan-break mottled-back" role="contentinfo">

	<div class="container wrap">
		<?php if( !is_home() && !is_front_page() ) : ?>
			<nav class="collapse navbar-collapse" role="navigation">
		      <?php get_template_part('templates/content', 'nav-diamonds'); ?>
			</nav>
		<?php endif; ?>
	
	</div>
		<p class="copyright">&copy; <?php echo date('Y'); ?> <?php echo strtoupper( get_bloginfo( 'name' ) ); ?> | <a href='https://graphicfusiondesign.com/'>DESIGNED BY GRAPHIC FUSION DESIGN</a></p>

</footer>
