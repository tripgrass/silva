<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
	<header class="article-header">
		<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
	</header>

	<div class='diamond-line'>
		<div class='small-d-wrap'>
			<div class='small-diamond'></div>
			<div class='small-diamond'></div>
			<div class='small-diamond'></div>
			<div class='small-diamond'></div>
		</div>
	</div>

	<?php the_content( ); ?>
<?php endwhile; endif; ?>
